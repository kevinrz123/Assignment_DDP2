package javari.park;

import java.util.ArrayList;
import java.util.List;

/**
 * Class ini merepresentasikan tiket yang dapat dipesan oleh visitor
 *
 * @author Kevin Raikhan Zain
 *
 *
 */

public class Register implements Registration {

    private static int idCounter = 1;
    private String visitorName;
    private int id;
    private ArrayList<SelectedAttraction> atraksi = new ArrayList<>();

    /**
     * Defines the base constructor for instantiating an object of
     * {@code Register}.
     *
     * @param visitorName nama visitor yang akan menonton atraksi
     * @param atraksi arrayList atraksi yang akan di tonton
     * @param id adalah id tiket
     */
    public Register(String visitorName, ArrayList<SelectedAttraction> atraksi) {
        this.visitorName = visitorName;
        this.atraksi = atraksi;
        this.id = idCounter;
        idCounter++;
    }

    /**
     * Returns the unique ID that associated with visitor's registration
     * in watching an attraction.
     *
     * @return
     */
    @Override
    public int getRegistrationId() {
        return id;
    }

    /**
     * Returns the name of visitor that associated with the registration.
     *
     * @return
     */
    @Override
    public String getVisitorName() {
        return visitorName;
    }

    /**
     * Changes visitor's name in the registration.
     *
     * @param name name of visitor
     * @return
     */
    @Override
    public String setVisitorName(String name) {
        visitorName = name;

        return name;
    }

    /**
     * Returns the list of all attractions that will be watched by the
     * visitor.
     *
     * @return
     */
    @Override
    public List<SelectedAttraction> getSelectedAttractions() {
        return atraksi;
    }

    /**
     * Adds a new attraction that will be watched by the visitor.
     *
     * @param selected the attraction
     * @return {@code true} if the attraction is successfully added into the
     * list, {@code false} otherwise
     */
    @Override
    public boolean addSelectedAttraction(SelectedAttraction selected) {
        return atraksi.add(selected);
    }
}
