package javari.park;

import javari.animal.Animal;
import java.util.List;

/**
 * Class ini merepresentasikan atraksi yang dapat dipilih visitor
 *
 * @author Kevin Raikhan Zain
 *
 *
 */

public class Attraction implements SelectedAttraction {

    private String name, type;
    private List<Animal> animPerformer;

    /**
     * Defines the base constructor for instantiating an object of
     * {@code Attraction}.
     *
     * @param name adalah nama atraksi
     * @param type adalah tipe animal yang akan atraksi
     * @param animPerformer adalah semua objek hewan yang akan perform
     */
    public Attraction(String name, String type, List<Animal> animPerformer) {
        this.name = name;
        this.type = type;
        this.animPerformer = animPerformer;
    }

    /**
     * Returns the name of attraction.
     *
     * @return
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Returns ths type of animal(s) that performing in this attraction.
     *
     * @return
     */
    @Override
    public String getType() {
        return type;
    }

    /**
     * Returns all performers of this attraction.
     *
     * @return
     */
    @Override
    public List<Animal> getPerformers() {
        return animPerformer;
    }

    /**
     * Adds a new animal into the list of performers.
     *
     * @param performer an instance of animal
     * @return {@code true} if the animal is successfully added into list of
     * performers, {@code false} otherwise
     */
    @Override
    public boolean addPerformer(Animal performer) {

        if (!performer.equals(null)) {
            animPerformer.add(performer);
            return true;
        }

        return false;
    }
}
