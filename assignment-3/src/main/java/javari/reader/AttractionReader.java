package javari.reader;


import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

/**
 * Class ini merepresentasikan objek yang akan membaca csv dan menghitung berapa attraction yang valid
 *
 * @author Kevin Raikhan Zain
 *
 *
 */

public class AttractionReader extends CsvReader {

    private int valid, inValid, countingMasterInvalid, circleOfFireInvalid, passionateCoderInvalid, dancingAnimalInvalid;

    private ArrayList<String> circleOfFire = new ArrayList<>();
    private ArrayList<String> countingMaster = new ArrayList<>();
    private ArrayList<String> passionateCoder = new ArrayList<>();
    private ArrayList<String> dancingAnimal = new ArrayList<>();


    /**
     * Defines the base constructor for instantiating an object of
     * {@code AttractionReader}.
     *
     * @param file path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *                     properly
     */
    public AttractionReader(Path file) throws IOException {
        super(file);
        String species, attraction;

        circleOfFire.add("Lion");
        circleOfFire.add("Whale");
        circleOfFire.add("Eagle");

        countingMaster.add("Hamster");
        countingMaster.add("Whale");
        countingMaster.add("Parrot");

        passionateCoder.add("Hamster");
        passionateCoder.add("Cat");
        passionateCoder.add("Snake");

        dancingAnimal.add("Parrot");
        dancingAnimal.add("Cat");
        dancingAnimal.add("Snake");
        dancingAnimal.add("Hamster");

        for (String i : lines) {

            species = i.split(COMMA)[0];
            attraction = i.split(COMMA)[1];
            checkAttraction(species, attraction);

        }
        inValid = countingMasterInvalid + passionateCoderInvalid + dancingAnimalInvalid + circleOfFireInvalid;
        valid = 4 - inValid;

    }


    // Check apakah suatu spesies bisa atraksi tertentu atau tidak

    /**
     * cek apakah suatu speies bisa atraksi tertentu atau tidak
     *
     * @param species adalah spesies yang akan ber atraksi
     * @param attraction adalah nama atraksinya
     */
    public void checkAttraction(String species, String attraction) {

        switch (attraction) {
            case "Circles of Fires":
                if (!looper(circleOfFire, species)) {
                    circleOfFireInvalid = 1;
                }
                break;
            case "Dancing Animals":
                if (!looper(dancingAnimal, species)) {
                    dancingAnimalInvalid = 1;
                }
                break;
            case "Counting Masters":
                if (!looper(countingMaster, species)) {
                    countingMasterInvalid = 1;
                }
                break;

            case "Passionate Coders":
                if (!looper(passionateCoder, species)) {
                    passionateCoderInvalid = 1;
                }
                break;
        }

    }


    /**
     * Method Helper untuk loop arrayList
     *
     * @param attraction arrayList hewan yang bisa melakukan atraksi
     * @param spesies nama spesies yang akan melakukan atraksi
     * @return
     */
    public boolean looper(ArrayList<String> attraction, String spesies) {
        for (String i : attraction) {
            if (i.equals(spesies)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Counts the number of valid attraction from read CSV file.
     *
     * @return
     */
    @Override
    public long countValidRecords() {
        return valid;
    }

    /**
     * Counts the number of invalid attraction from read CSV file.
     *
     * @return
     */
    @Override
    public long countInvalidRecords() {
        return inValid;
    }
}
