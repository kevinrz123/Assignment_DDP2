package javari.reader;

import javari.DataStorage;

import java.io.IOException;
import java.nio.file.Path;

/**
 * Class ini merepresentasikan objek yang akan membaca csv dan menghitung berapa animal yang valid
 *
 * @author Kevin Raikhan Zain
 *
 *
 */

public class RecordReader extends CsvReader {

    private static int counter = 1;
    private int valid, invalid;

    /**
     * Defines the base constructor for instantiating an object of
     * {@code RecordReader}.
     *
     * @param file path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *                     properly
     */
    public RecordReader(Path file) throws IOException {
        super(file);
        String id, animal, name, gender, length, weight, specialStatus, condition;


        for (String i : lines) {
            String[] baris = i.split(COMMA);
            id = baris[0];
            animal = baris[1];
            name = baris[2];
            gender = baris[3];
            length = baris[4];
            weight = baris[5];
            specialStatus = baris[6];
            condition = baris[7];

            if (counter != Integer.parseInt(id)) {
                invalid++;
            }

            DataStorage.addAnimal(id, animal, name, gender, length, weight, specialStatus, condition);
            counter++;

        }
    }

    /**
     * Counts the number of valid records from read CSV file.
     *
     * @return
     */
    @Override
    public long countValidRecords() {
        return (counter - 1) - invalid;
    }

    /**
     * Counts the number of invalid records from read CSV file.
     *
     * @return
     */
    @Override
    public long countInvalidRecords() {
        return invalid;
    }
}
