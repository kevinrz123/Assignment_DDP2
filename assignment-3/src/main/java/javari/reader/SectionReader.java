package javari.reader;


import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

/**
 * Class ini merepresentasikan objek yang akan membaca csv dan menghitung berapa section dan kategori yang valid
 *
 * @author Kevin Raikhan Zain
 *
 *
 */

public class SectionReader extends CsvReader {

    private int validSection, invalidSection, avesInvalid, mammalInvalid, reptileInvalid, worldAvesInvalid,
            exploreTheMammalInvalid, reptileKingdomInvalid, validKategori, invalidKategori;
    private ArrayList<String> worldOfAves = new ArrayList<>();
    private ArrayList<String> exploreTheMammals = new ArrayList<>();
    private ArrayList<String> reptilianKingdom = new ArrayList<>();


    /**
     * Defines the base constructor for instantiating an object of
     * {@code SectionReader}.
     *
     * @param file path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *                     properly
     */
    public SectionReader(Path file) throws IOException {
        super(file);
        String species, kategori, section;

        worldOfAves.add("Parrot");
        worldOfAves.add("Eagle");

        exploreTheMammals.add("Hamster");
        exploreTheMammals.add("Whale");
        exploreTheMammals.add("Cat");
        exploreTheMammals.add("Lion");

        reptilianKingdom.add("Snake");

        for (String i : lines) {

            species = i.split(COMMA)[0];
            kategori = i.split(COMMA)[1];
            section = i.split(COMMA)[2];

            checkSection(species, section);
            checkCategory(species, kategori);
        }

        invalidSection = worldAvesInvalid + exploreTheMammalInvalid + reptileKingdomInvalid;
        invalidKategori = mammalInvalid + avesInvalid + reptileInvalid;

        validSection = 3 - invalidSection;
        validKategori = 3 - invalidKategori;
    }

    // Getter

    public ArrayList<String> getExploreTheMammals() {
        return exploreTheMammals;
    }

    public ArrayList<String> getReptilianKingdom() {
        return reptilianKingdom;
    }

    public ArrayList<String> getWorldOfAves() {
        return worldOfAves;
    }


    /**
     * Mengecek berapa section yang valid dan invalid
     *
     * @param species spesies animal yang di cek
     * @param section cek spesies di section apa
     */

    private void checkSection(String species, String section) {

        switch (section) {
            case "Explore the Mammals":
                if (!looper(species, exploreTheMammals)) {
                    exploreTheMammalInvalid = 1;
                }
                break;
            case "World of Aves":
                if (!looper(species, worldOfAves)) {
                    worldAvesInvalid = 1;
                }
                break;

            case "Reptillian Kingdom":
                if (!looper(species, reptilianKingdom)) {
                    reptileKingdomInvalid = 1;
                }
                break;
        }
    }

    /**
     * check berapa kategori yang valid dan invalid dengan mencocokan spesies dan kategori
     *
     * @param species spesies hewan apa
     * @param kategori kategori hewan apa
     * @return
     */
    private boolean checkCategory(String species, String kategori) {
        switch (kategori) {
            case "mammals":
                if (!looper(species, exploreTheMammals)) {
                    mammalInvalid = 1;
                }
                break;
            case "aves":
                if (!looper(species, worldOfAves)) {
                    avesInvalid = 1;
                }
                break;

            case "reptiles":
                if (!looper(species, reptilianKingdom)) {
                    reptileInvalid = 1;
                }
                break;
        }

        return false;
    }



    /**
     * Method helper untuk loop arrayList section dan return true jika ada spesies tersebut
     *
     * @param species nama spesies hewan yang dicari
     * @param section Di cari di section mana
     * @return true jika ada spesies tersebut di arrayList section
     */
    private boolean looper(String species, ArrayList<String> section) {

        for (String i : section) {
            if (i.equals(species)) {
                return true;
            }
        }
        return false;
    }


    /**
     * Counts the number of valid section from read CSV file.
     *
     * @return
     */
    @Override
    public long countValidRecords() {
        return validSection;

    }

    /**
     * Counts the number of invalid Section from read CSV file.
     *
     * @return
     */
    @Override
    public long countInvalidRecords() {
        return invalidSection;
    }

    public long countValidKategori() {
        return validKategori;
    }

    public long countInvalidKategori() {
        return invalidKategori;
    }
}