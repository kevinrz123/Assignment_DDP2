package javari;

import javari.animal.Animal;
import javari.park.Attraction;
import javari.park.Register;
import javari.park.SelectedAttraction;
import javari.reader.AttractionReader;
import javari.reader.RecordReader;
import javari.reader.SectionReader;
import javari.writer.RegistrationWriter;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;
/**
 * Class ini adalah class main dari JavariPark
 *
 * @author Kevin Raikhan Zain
 * Diskusi dengan Stefanus Khrisna dan M. Feril Bagus
 *
 */
public class JavariPark {
    public static void main(String[] args) throws IOException {
        Scanner input = new Scanner(System.in);
        System.out.println("\nWelcome to Javari Park Festival - Registration Service!\n");
        System.out.println("... Opening default section database from data. ... File not found or incorrect file!");
        System.out.print("Please provide the source data path: ");

        // Minta input tempat file
        String jalur = input.nextLine(); // Nanti diganti jadi ini
//        String jalur = "C:\\Users\\kevin\\Documents\\Kuliah\\DDP 2 Java and Git\\Assignment"; // Sementara

        System.out.println("\n... Loading... Success... System is populating data...\n");

        AttractionReader attractReader = new AttractionReader(Paths.get(jalur, "Assignment_DDP2\\assignment-3\\data\\animals_attractions.csv"));
        SectionReader sectReader = new SectionReader(Paths.get(jalur, "Assignment_DDP2\\assignment-3\\data\\animals_categories.csv"));
        RecordReader recRead = new RecordReader(Paths.get(jalur, "Assignment_DDP2\\assignment-3\\data\\animals_records.csv"));

        System.out.printf("Found %d valid sections and %d invalid sections\n", sectReader.countValidRecords(), attractReader.countInvalidRecords());
        System.out.printf("Found %d valid attractions and %d invalid attractions\n", attractReader.countValidRecords(), attractReader.countInvalidRecords());
        System.out.printf("Found %d valid animal categories and %d invalid animal categories\n", sectReader.countValidKategori(), sectReader.countInvalidKategori());
        System.out.printf("Found %d valid animal records and %d invalid animal records\n\n", recRead.countValidRecords(), recRead.countInvalidRecords());

        System.out.println("Welcome to Javari Park Festival - Registration Service!\n");

        System.out.println("Javari Park has 3 sections:");
        System.out.println("1. Explore the Mammals");
        System.out.println("2. World of Aves");
        System.out.println("3. Reptilian Kingdom");
        System.out.print("Please choose your preferred section (type the number): ");
        int chooseSection = Integer.parseInt(input.nextLine());
        System.out.println();

        ArrayList<String> sectionChosen = new ArrayList<>();
        String kategoriChosen = "";

        // Memilih section
        switch (chooseSection) {
            case 1:
                System.out.println("--Explore the Mammals--");
                for (int i = 0; i < sectReader.getExploreTheMammals().size(); i++) {
                    System.out.println((i + 1) + ". " + sectReader.getExploreTheMammals().get(i));
                }
                sectionChosen = sectReader.getExploreTheMammals();
                kategoriChosen = "Mammal";
                break;
            case 2:
                System.out.println("--World of Aves--");
                for (int i = 0; i < sectReader.getWorldOfAves().size(); i++) {
                    System.out.println((i + 1) + ". " + sectReader.getWorldOfAves().get(i));
                }
                sectionChosen = sectReader.getWorldOfAves();
                kategoriChosen = "Aves";
                break;
            case 3:
                System.out.println("--Reptilian Kingdom--");
                for (int i = 0; i < sectReader.getReptilianKingdom().size(); i++) {
                    System.out.println((i + 1) + ". " + sectReader.getReptilianKingdom().get(i));
                }
                sectionChosen = sectReader.getReptilianKingdom();
                kategoriChosen = "Reptile";
                break;
        }

        System.out.print("Please choose your preferred animals (type the number): ");

        int animalChosen;

        while (true) {
            // Memilih animal
            String masuk = input.nextLine();
            animalChosen = Integer.parseInt(masuk);

            // Sementara untuk exit tulis angka "99"
            if (masuk.equals("99")) {
                break;
            } else if (DataStorage.findShowable(sectionChosen.get(animalChosen - 1)).size() == 0) {
                System.out.println("Unfortunately, no <Animal> can perform any attraction, please choose other animals");
            } else {
                break;
            }
        }

        System.out.println();

        String speciesChosen = sectionChosen.get(animalChosen - 1);

        System.out.println("--" + speciesChosen + "--");
        for (int x = 0; x < DataStorage.getAttractionAnimal(speciesChosen).size(); x++) {
            System.out.println((x + 1) + ". " + DataStorage.getAttractionAnimal(speciesChosen).get(x));
        }


        System.out.print("Please choose your preferred attractions (type the number): ");

        int attractionChosen = Integer.parseInt(input.nextLine());
        System.out.println();

        System.out.println("Wow, one more step,\nplease let us know your name: ");
        String custName = input.nextLine();

        // Atraksi yang dipilih
        ArrayList<SelectedAttraction> atraksiDipilih = new ArrayList<>();

        atraksiDipilih.add(new Attraction(DataStorage.getAttractionAnimal(speciesChosen).get(attractionChosen - 1), speciesChosen, DataStorage.findShowable(speciesChosen)));
        System.out.println("Yeay, final check!\nHere is your data, and the attraction you chose:\n");

        // Membuat register
        Register regist = new Register(custName, atraksiDipilih);

        // Print nama visitor
        System.out.println("Name: " + regist.getVisitorName());
        System.out.print("Attractions: ");

        // print semua hewan yang melakukan atraksi yang dipilih visitor
        for (SelectedAttraction i : regist.getSelectedAttractions()) {
            System.out.println(i.getName() + " -> " + i.getType());
            System.out.print("With: ");
            for (Animal x : i.getPerformers()) {
                System.out.print(x.getName() + ", ");
            }
            System.out.println();
        }
        // Membuat json
        RegistrationWriter.writeJson(regist, Paths.get("C:\\Users\\kevin\\Documents\\Kuliah\\DDP 2 Java and Git\\Assignment\\Assignment_DDP2\\assignment-3\\src\\main\\java\\javari"));
    }
}