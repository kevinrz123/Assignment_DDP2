package javari;


import javari.animal.*;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Class ini berisi ArrayList semua objek Animal yang telah dibuat dan method untuk mengolah data tersebut
 *@author Kevin Raikhan Zain
 */

public class DataStorage {

    // ArrayList Semua Animal yang sudah dibuat
    private static ArrayList<Aves> worldOfAves = new ArrayList<>();
    private static ArrayList<Mammal> exploreTheMammal = new ArrayList<>();
    private static ArrayList<Reptile> reptilianKingdom = new ArrayList<>();

    /**
     * Membuat objek dan menambahkannya ke arrayList
     *
     * @param id id hewan
     * @param animal jenis spesies hewan
     * @param gender jenis kelamin hewan
     * @param length panjang tubuh hewan
     * @param weight berat tubuh hewan
     * @param specialStatus status hewan, jika mammal pregnant, reptile tame, aves layEgg
     * @param condition kondisi hewan sehat atau tidak
     * @return void
     */
    public static void addAnimal(String id, String animal, String name, String gender, String length, String weight, String specialStatus, String condition) {

        if (animal.equals("Hamster") || animal.equals("Cat") || animal.equals("Whale")) {
            exploreTheMammal.add(new Mammal(Integer.parseInt(id), animal, name, Gender.parseGender(gender), Double.parseDouble(length), Double.parseDouble(weight), specialStatus, Condition.parseCondition(condition)));
        } else if (animal.equals("Lion")) {
            exploreTheMammal.add(new Lion(Integer.parseInt(id), animal, name, Gender.parseGender(gender), Double.parseDouble(length), Double.parseDouble(weight), specialStatus, Condition.parseCondition(condition)));
        } else if (animal.equals("Parrot") || animal.equals("Eagle")) {
            worldOfAves.add(new Aves(Integer.parseInt(id), animal, name, Gender.parseGender(gender), Double.parseDouble(length), Double.parseDouble(weight), specialStatus, Condition.parseCondition(condition)));
        } else if (animal.equals("Snake")) {
            reptilianKingdom.add(new Reptile(Integer.parseInt(id), animal, name, Gender.parseGender(gender), Double.parseDouble(length), Double.parseDouble(weight), specialStatus, Condition.parseCondition(condition)));
        }
    }


    /**
     * mencari hewan yang dapat melakukan atraksi berdasarkan spesies
     *
     * @param anim spesies hewan
     * @return ArrayList semua hewan anim yang dapat melakukan atraksi
     */
    public static ArrayList<Animal> findShowable(String anim) {
        ArrayList<Animal> showable = new ArrayList<>();

        switch (anim) {
            case "Cat":
                for (Animal i : exploreTheMammal) {
                    if (i.getType().equals("Cat") && i.isShowable()) {
                        showable.add(i);
                    }
                }
                break;
            case "Lion":
                for (Animal i : exploreTheMammal) {
                    if (i.getType().equals("Lion") && i.isShowable()) {
                        showable.add(i);
                    }
                }
                break;
            case "Hamster":
                for (Animal i : exploreTheMammal) {
                    if (i.getType().equals("Hamster") && i.isShowable()) {
                        showable.add(i);
                    }
                }
                break;
            case "Whale":
                for (Animal i : exploreTheMammal) {
                    if (i.getType().equals("Whale") && i.isShowable()) {
                        showable.add(i);
                    }
                }
                break;
            case "Parrot":
                for (Animal i : exploreTheMammal) {
                    if (i.getType().equals("Parrot") && i.isShowable()) {
                        showable.add(i);
                    }
                }
                break;
            case "Eagle":
                for (Animal i : exploreTheMammal) {
                    if (i.getType().equals("Eagle") && i.isShowable()) {
                        showable.add(i);
                    }
                }
                break;
            case "Snake":
                for (Animal i : exploreTheMammal) {
                    if (i.getType().equals("Snake") && i.isShowable()) {
                        showable.add(i);
                    }
                }
                break;
        }
        return showable;
    }

    /**
     * mengambil arrayList atraksi yang dapat dilakukan suatu spesies
     *
     * @param anim spesies hewan yang ingin di cari atraksinya
     * @return semua atraksion yang dapat dilakukan suatu anim
     */
    public static ArrayList<String> getAttractionAnimal(String anim) {
        ArrayList<String> catAttraction = new ArrayList<>();
        ArrayList<String> whaleAttraction = new ArrayList<>();
        ArrayList<String> lionAttraction = new ArrayList<>();
        ArrayList<String> hamsterAttraction = new ArrayList<>();
        ArrayList<String> parrotAttraction = new ArrayList<>();
        ArrayList<String> eagleAttraction = new ArrayList<>();
        ArrayList<String> snakeAttraction = new ArrayList<>();

        catAttraction.addAll(Arrays.asList("Dancing Animals", "Passionate Coders"));
        whaleAttraction.addAll(Arrays.asList("Circle of Fire", "Counting Masters"));
        lionAttraction.add("Circle of Fire");
        hamsterAttraction.addAll(Arrays.asList("Dancing Animals", "Counting Masters", "Passionate Coders"));
        parrotAttraction.addAll(Arrays.asList("Dancing Animals", "Counting Masters"));
        eagleAttraction.addAll(Arrays.asList("Circle of Fire"));
        snakeAttraction.addAll(Arrays.asList("Dancing Animals", "Passionate Coders"));

        ArrayList<String> attract = new ArrayList<>();

        switch (anim) {
            case "Cat":
                return catAttraction;
            case "Hamster":
                return hamsterAttraction;
            case "Whale":
                return whaleAttraction;
            case "Lion":
                return lionAttraction;
            case "Parrot":
                return parrotAttraction;
            case "Eagle":
                return eagleAttraction;
            case "Snake":
                return snakeAttraction;
        }
        return attract;
    }
}
