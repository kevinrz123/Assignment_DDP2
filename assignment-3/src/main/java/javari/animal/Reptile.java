package javari.animal;

/**
 * Class ini merepresentasikan atribut dan behavior Reptile (Snake)
 *
 * @author Kevin Raikhan Zain
 *
 *
 */

public class Reptile extends Animal {

    private boolean isTame;

    /**
     * Defines the base constructor for instantiating an object of
     * {@code Lion}.
     *
     * @param id id setiap hewan
     * @param type spesies hewan
     * @param name nama hewan tersebut
     * @param gender jenis kelamin hewan tersebut
     * @param length panjang tubuh hewan
     * @param weight berat hewan
     * @param tame apakah dia sudah tame tau belum
     * @param condition kondisi healthy atau not healthy
     */
    public Reptile(Integer id, String type, String name, Gender gender, double length, double weight, String tame, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        isTame = tame.equals("tame") ? true : false;

    }

    /**
     * Performs more specific checking to know whether an animal is able
     * to perform or not.
     *
     * @return true jika sudah jinak atau tame == true;
     */
    @Override
    protected boolean specificCondition() {
        return isTame;
    }
}
