package javari.animal;

/**
 * Class ini merepresentasikan atribut dan behavior Mammal (Cat, Hamster, Lion, Wale)
 *
 * @author Kevin Raikhan Zain
 *
 *
 */

public class Mammal extends Animal {

    private boolean isPregnant;

    /**
     * Defines the base constructor for instantiating an object of
     * {@code Lion}.
     *
     * @param id id setiap hewan
     * @param type spesies hewan
     * @param name nama hewan tersebut
     * @param gender jenis kelamin hewan tersebut
     * @param length panjang tubuh hewan
     * @param weight berat hewan
     * @param pregnant apakah dia pregnant atau tidak
     * @param condition kondisi healthy atau not healthy
     */
    public Mammal(Integer id, String type, String name, Gender gender, double length, double weight, String pregnant, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        isPregnant = pregnant.equals("pregnant") ? true : false;

    }

    /**
     * return kondisi dia pregnant atau tidak
     * @return true jika dia tidak pregnant
     */
    @Override
    protected boolean specificCondition() {
        return !isPregnant;
    }
}
