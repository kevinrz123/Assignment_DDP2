package javari.animal;

/**
 * Class ini merepresentasikan atribut dan behavior Lion
 *
 * @author Kevin Raikhan Zain
 *
 *
 */

public class Lion extends Mammal {
    /**
     * Defines the base constructor for instantiating an object of
     * {@code Lion}.
     *
     * @param id id setiap hewan
     * @param type spesies hewan
     * @param name nama hewan tersebut
     * @param gender jenis kelamin hewan tersebut
     * @param length panjang tubuh hewan
     * @param weight berat hewan
     * @param pregnant apakah dia pregnant atau tidak
     * @param condition kondisi healthy atau not healthy
     */
    public Lion(Integer id, String type, String name, Gender gender, double length, double weight, String pregnant, Condition condition) {
        super(id, type, name, gender, length, weight, pregnant, condition);

    }

    /**
     * jika dia male maka bisa atraksi
     *
     * @return true jika geder male
     */
    @Override
    protected boolean specificCondition() {

        if (getGender().equals("MALE")) {
            return true;
        }
        return false;

    }
}
