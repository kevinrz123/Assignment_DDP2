package javari.animal;

/**
 * Class ini merepresentasikan atribut dan behavior Aves (Parrot, Eagle)
 *
 * @author Kevin Raikhan Zain
 *
 *
 */

public class Aves extends Animal {

    private boolean layEgg;

    /**
     * Defines the base constructor for instantiating an object of
     * {@code Aves}.
     *
     * @param id id setiap hewan
     * @param type spesies hewan
     * @param name nama hewan tersebut
     * @param gender jenis kelamin hewan tersebut
     * @param length panjang tubuh hewan
     * @param weight berat hewan
     * @param layEgg apakah dia sedang bertelur atau tidak
     * @param condition kondisi healthy atau not healthy
     */
    public Aves(Integer id, String type, String name, Gender gender, double length, double weight, String layEgg, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.layEgg = layEgg.equals("tame") ? true : false;
    }

    /**
     * Performs more specific checking to know whether an animal is able
     * to perform or not.
     *
     * @return true jika sedang tidak bertelur false jika sedang bertelur
     */
    @Override
    protected boolean specificCondition() {
        return !layEgg;
    }
}
