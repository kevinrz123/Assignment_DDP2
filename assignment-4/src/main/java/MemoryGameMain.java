import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Class yang berisi method main
 */
public class MemoryGameMain extends JFrame {
    /**
     * Panel yang berisi tombol tombol dan tempat game berlangsung
     */
    private static GamePanel gem;

    /**
     * Jumlah try
     */
    private static int tries;

    /**
     * JLabel Tulisan jumlah try
     */
    private static JLabel numbTriesLabel;

    /**
     * Panel yang berisi Label numbTriesLabel dan tombol Reset
     */
    private static JPanel buttonAndStats;


    /**
     * Constructor
     */
    public MemoryGameMain() {
        super("Memory Game Dek Depe");
        setLayout(new FlowLayout());
        tries = 0;
        gem = new GamePanel();
        buttonAndStats = new JPanel(new GridLayout(2, 1));
        JButton resetBut = new JButton("          Reset          ");
        numbTriesLabel = new JLabel("Number of Tries : " + tries);
        resetBut.setSize(100, 100);

        /**
         * Action yang dilakukan tombol reset menjalankan method reset()
         */
        resetBut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                reset();
            }
        });
        add(gem);
        add(buttonAndStats);
        buttonAndStats.add(resetBut);
        buttonAndStats.add(numbTriesLabel);


        setVisible(true);
        pack();
    }

    /**
     * Main Method
     */
    public static void main(String[] args) {

        MemoryGameMain game = new MemoryGameMain();
        game.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    /**
     * Method untuk melakukan reset game
     */
    public static void reset() {
        gem.reset();
        tries = 0;
        numbTriesLabel.setText("Number of Tries : " + tries);
    }

    /**
     * Method untuk dijalankan ketika membuka 2 gambar
     */
    public static void addTries() {
        tries++;
        numbTriesLabel.setText("Number of Tries : " + tries);
    }
}
