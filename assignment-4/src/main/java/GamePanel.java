import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Class yang merepresentasikan panel yang berisi tombol tombol dan cara kerja game
 */
public class GamePanel extends JPanel {

    /**
     * jumlah yang sudah di pencet
     */
    private int pressed = 0;
    /**
     * Jumlah benar
     */
    private int correct = 0;

    private JToggleButton pressedButton;

    /**
     * Constructor
     */

    public GamePanel() {
        super(new GridLayout(6, 6));

        ArrayList<ImageIcon> gambars = new ArrayList<>();

        // Angka untuk random
        ArrayList<Integer> angkas1 = new ArrayList<>();
        ArrayList<Integer> angkas2 = new ArrayList<>();

        // Menambahkan angka ke arrayList
        for (int i = 0; i < 18; i++) {
            angkas1.add(i);
            angkas2.add(i);
        }

        // Random angka
        Collections.shuffle(angkas1);
        Collections.shuffle(angkas2);

        // Menambahkan gambar ke ArrayList
        for (int i = 1; i <= 18; i++) {
            gambars.add(new ImageIcon("../../../img/anim" + i + ".jpg"));
        }

        for (int i = 0; i < 36; i++) {

            JToggleButton tombol = new JToggleButton(new ImageIcon("../../../img/depan.png"));
            if (i < 18) {
                tombol.setSelectedIcon(gambars.get(angkas1.get(i)));
            } else {
                tombol.setSelectedIcon(gambars.get(angkas2.get(i - 18)));
            }

            /**
             * actionListener untuk setiap tombol
             */
            ////////////////////////////////////////
            tombol.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    pressed++;

                    /**
                     * Jika sudah 2 yang di tekan
                     */
                    if (pressed == 2) {
                        MemoryGameMain.addTries();

                        // Jika Gambar sama dan tombol kedua bukan dirinya sendii
                        if (tombol.getSelectedIcon().equals(pressedButton.getSelectedIcon()) && !tombol.equals(pressedButton)) {
                            correct++;
                            pressed = 0;
                            tombol.setEnabled(false);
                            pressedButton.setEnabled(false);

                            // Jika menang do something
                            if (correct == 18) {
                                //TODO
                                System.out.println("YEAY MEANNG");
                                JOptionPane.showMessageDialog(null, "Selamat, Anda Menang!1!1!1!");
                            }
                            // Delay sebelum menutup gambar
                            try {
                                Thread.sleep(150);
                            } catch (Exception y) {
                                System.out.println(y);
                            }

                        } else {
                            // Jika tidak sama dan tombol kedua bukan dirinya sendiri
                            tombol.setSelected(false);
                            pressedButton.setSelected(false);
                            pressed = 0;


                            // Tunggu 1 detik sebelum tutup
                            try {
                                Thread.sleep(500);
                            } catch (Exception y) {
                                System.out.println(y);
                            }
                        }

                    } else {
                        pressedButton = tombol;
                    }

                }
            });


            ////////////////////////////////////////


            add(tombol);
        }
    }

    /**
     * Method reset untuk menutup semua tombol dan mereset correct
     */
    public void reset() {
        for (Component i : getComponents()) {
            JToggleButton tbl = (JToggleButton) i;
            i.setEnabled(true);
            tbl.setSelected(false);
        }
    }

}