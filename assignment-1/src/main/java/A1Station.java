import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms

    // You can add new variables or methods in this class


    public static void main(String[] args) {
        // TODO Complete me!
        int jumlahKretaSementara = 0;
        Scanner input = new Scanner(System.in);

        int jumlahKucing = Integer.parseInt(input.nextLine());

        String[] kucingArray = input.nextLine().split(",");
        double berat = Double.valueOf(kucingArray[1]);
        double panjang = Double.valueOf(kucingArray[2]);
        String nama = kucingArray[0];

        WildCat meong = new WildCat(nama, berat, panjang);
        TrainCar kreta = new TrainCar(meong);

        for (int i = 1; i < jumlahKucing; i++) {
            jumlahKretaSementara++;

            kucingArray = input.nextLine().split(",");
            berat = Double.valueOf(kucingArray[1]);
            panjang = Double.valueOf(kucingArray[2]);
            nama = kucingArray[0];

            WildCat catNext = new WildCat(nama, berat, panjang);
            TrainCar kretaNext = new TrainCar(catNext, kreta);

            if (kreta.computeTotalWeight() > THRESHOLD) {
                TrainCar.setJumlahKereta(jumlahKretaSementara);
                kreta.printCarDisp();
                kretaNext.setNext(null);
                TrainCar.setJumlahKereta(1);
                jumlahKretaSementara = 0;

            }
            kreta = kretaNext;

        }
        kreta.printCarDisp();

    }

}
