public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms
    private WildCat cat;
    private TrainCar next;
    private static int jumlahKereta = 0;

    // TODO Complete me!

    public TrainCar(WildCat cat) {
        // TODO Complete me!
        jumlahKereta++;
        this.cat = cat;
        this.next = null;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        // TODO Complete me!
        jumlahKereta++;
        this.cat = cat;
        this.next = next;
    }

    public static void setJumlahKereta(int jumlahKereta) {
        TrainCar.jumlahKereta = jumlahKereta;
    }

    public void setNext(TrainCar x) {
        next = x;
    }

    public WildCat getCat() {
        return cat;
    }

    public double computeTotalWeight() {
        // TODO Complete me!
        if (this.next == null) {
            return EMPTY_WEIGHT + cat.weight;
        } else {
            return (EMPTY_WEIGHT + cat.weight) + next.computeTotalWeight();
        }

    }

    public double computeTotalMassIndex() {
        // TODO Complete me!
        if (next == null) {
            return cat.computeMassIndex();
        } else {
            return cat.computeMassIndex() + next.computeTotalMassIndex();
        }
    }


    public void printCar() {
        // TODO Complete me!
        System.out.println(rekursi());

    }

    public void printCarDisp() {
        // TODO Complete me!

        System.out.println("The train departs to Javari Park");
        System.out.println("[LOCO]<--" + rekursi());
        System.out.printf("Average mass index of all cats: %.2f", computeTotalMassIndex() / jumlahKereta);

        String status = "normal";
        if (computeTotalMassIndex() / jumlahKereta <= 18.5) {
            status = "underweight";
        } else if (computeTotalMassIndex() / jumlahKereta >= 25 && computeTotalMassIndex() / jumlahKereta < 30) {
            status = "overweight";
        } else if (computeTotalMassIndex() / jumlahKereta >= 30) {
            status = "obese";
        }

        System.out.printf("\nIn average, the cats in the train are *%s*\n", status);
    }

    public String rekursi() {
        if (next == null) {
            return "(" + cat.name + ")";
        } else {
            return "(" + cat.name + ")--" + next.rekursi();
        }
    }
}
