package cage;

import animal.Animal;

public class Cage {
    protected String tipeKandang;
    protected Animal hewan;

    public Cage(Animal hewan) {
        this.hewan = hewan;
    }

    public Animal getHewan() {
        return hewan;
    }

    @Override
    public String toString() {
        return String.format("%s(%d - %s)", hewan.getName(), hewan.getLength(), tipeKandang);
    }

}
