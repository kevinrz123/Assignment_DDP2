package cage;

import animal.Animal;

public class CageOutdoor extends Cage {
    public CageOutdoor(Animal hewan) {
        super(hewan);
        tipeKandang = "B";
        if (hewan.getLength() < 75) {
            tipeKandang = "A";
        } else if (hewan.getLength() > 90) {
            tipeKandang = "C";
        }
    }
}
