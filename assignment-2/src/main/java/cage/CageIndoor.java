package cage;

import animal.Animal;

public class CageIndoor extends Cage {

    public CageIndoor(Animal hewan) {
        super(hewan);
        tipeKandang = "B";
        if (hewan.getLength() < 45) {
            tipeKandang = "A";
        } else if (hewan.getLength() > 60) {
            tipeKandang = "C";
        }
    }


}
