

package animal;

import java.util.Random;

public class Cat extends Animal {
    private static String[] behavior = {"1: Brush the fur", "2: Cuddle"};
    private static String[] voice = {"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};

    public Cat(int length, String name) {
        super(length, name);
    }

    public static String[] getBehavior() {
        return behavior;
    }

    public void brush() {
        Random generator = new Random();
        int randomIndex = generator.nextInt(voice.length);
        System.out.println(getName() + "makes a voice: " + voice[randomIndex]);
    }

    public void cuddle() {
        System.out.println(getName() + "makes a voice: Purrr..");
    }

    @Override
    public String toString() {
        return "Cat";
    }
}
