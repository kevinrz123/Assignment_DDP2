package animal;

public class Eagle extends Animal {

    private static String[] behavior = {"1: Order to fly"};

    public Eagle(int length, String name) {
        super(length, name);
    }

    public static String[] getBehavior() {
        return behavior;
    }

    public void OrderToFly() {
        System.out.println(getName() + " makes a voice: kwaakk...");
    }

    @Override
    public String toString() {
        return "Eagle";
    }
}
