package animal;

public class Hamster extends Animal {

    private static String[] behavior = {"1: See it gnawing", "2: Order to run in the hamster wheel"};


    public Hamster(int length, String name) {
        super(length, name);
    }

    public static String[] getBehavior() {
        return behavior;
    }

    public void gnaw() {
        System.out.println(getName() + " makes a voice: ngkkrit.. ngkkrrriiit");
    }

    public void hamsWheel() {
        System.out.println(getName() + " makes a voice: trrr.... trrr...");
    }

    @Override
    public String toString() {
        return "Hamster";
    }
}
