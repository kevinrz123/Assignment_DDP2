package animal;

public class Animal {
    private int length;
    private String name;

    public Animal(int length, String name) {
        this.length = length;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getLength() {
        return length;
    }
}