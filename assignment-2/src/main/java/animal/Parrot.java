package animal;

public class Parrot extends Animal {

    private static String[] behavior = {"1: Order to fly", "2: Do conversation"};

    public Parrot(int length, String name) {
        super(length, name);
    }

    public static String[] getBehavior() {
        return behavior;
    }

    public void fly() {
        System.out.println(getName() + " makes a voice: FLYYYY.....");
    }

    public void conversation(String omongan) {

        System.out.println(getName() + " says: " + omongan.toUpperCase());
    }
    // Kalo Perintah >= 3 maka parrot says HM
    public void doNothing() {
        System.out.println(getName() + " says: HM?");
    }


    @Override
    public String toString() {
        return "Parrot";
    }
}

