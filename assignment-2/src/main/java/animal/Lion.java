package animal;

public class Lion extends Animal {

    private static String[] behavior = {"1: See it hunting", "2: Brush the mane", "3: Disturb it"};

    public Lion(int length, String name) {
        super(length, name);
    }

    public static String[] getBehavior() {
        return behavior;
    }

    public void hunt() {
        System.out.println("Lion is hunting..\n" + getName() + " makes a voice: err...!");
    }

    public void brushMane() {
        System.out.println("Clean the lion’s mane..\n" + getName() + " makes a voice: Hauhhmm!");
    }

    public void disturb() {
        System.out.println(getName() + " makes a voice: HAUHHMM!");
    }

    @Override
    public String toString() {
        return "Lion";

    }
}
