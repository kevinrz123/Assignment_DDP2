import animal.*;
import cage.Cage;
import cage.CageIndoor;
import cage.CageOutdoor;

import java.util.ArrayList;

public class Storage {

    // ArrayList berisi semua hewan setiap spesies
    private static ArrayList<Cat> catArray = new ArrayList<>();
    private static ArrayList<Eagle> eagleArray = new ArrayList<>();
    private static ArrayList<Parrot> parrotArray = new ArrayList<>();
    private static ArrayList<Hamster> hamsterArray = new ArrayList<>();
    private static ArrayList<Lion> lionArray = new ArrayList<>();


    // ArrayList berisi kandang kandang hewan
    private static ArrayList<Cage> catList = new ArrayList<>();
    private static ArrayList<Cage> hamsterList = new ArrayList<>();
    private static ArrayList<Cage> lionList = new ArrayList<>();
    private static ArrayList<Cage> eagleList = new ArrayList<>();
    private static ArrayList<Cage> parrotList = new ArrayList<>();

    // Penyimpanan sementara untuk print before and after arrangement
    private static ArrayList<Cage> level1Temp = new ArrayList<>();
    private static ArrayList<Cage> level2Temp = new ArrayList<>();
    private static ArrayList<Cage> level3Temp = new ArrayList<>();


    // Method add hewan ke dalam ArrayList kandang hewan
    public static void addCat(Cat hewan) {
        catList.add(new CageIndoor(hewan));
        catArray.add(hewan);
    }

    public static void addLion(Lion hewan) {
        lionList.add(new CageOutdoor(hewan));
        lionArray.add(hewan);
    }

    public static void addEagle(Eagle hewan) {
        eagleList.add(new CageOutdoor(hewan));
        eagleArray.add(hewan);
    }

    public static void addParrot(Parrot hewan) {
        parrotList.add(new CageIndoor(hewan));
        parrotArray.add(hewan);
    }

    public static void addHamster(Hamster hewan) {
        hamsterList.add(new CageIndoor(hewan));
        hamsterArray.add(hewan);
    }

    // Print kandang before Arrangement
    public static void printBeforeArrangement(ArrayList<Cage> list) {
        int angka, level3, level2;

        System.out.println("\nBefore Arrangement \n");
        // Default jika hewan kurang dari 2 jadi ga masuk ke for loop yang level 2 dan 3
        level3 = 1;
        level2 = 1;

        // Jika hewan lebih dari samadengan 3
        if (list.size() >= 3) {
            angka = list.size() / 3;
            level3 = angka * 2;
            level2 = angka;

            // Jika hewan ada 2
        } else if (list.size() == 2) {
            level3 = list.size();
            level2 = 1;
        }

        int level1 = 0;

        // Print Level 3
        System.out.print("Level 3: ");
        for (int i = level3; i < list.size(); i++) {
            level3Temp.add(list.get(i));
            System.out.print(list.get(i));
        }
        System.out.println();

        // Print Level 2
        System.out.print("Level 2: ");
        for (int i = level2; i < level3; i++) {
            System.out.print(list.get(i) + ",");
            level2Temp.add(list.get(i));
        }
        System.out.println();

        // Print Level 1
        System.out.print("Level 1: ");
        for (int i = level1; i < level2; i++) {
            System.out.print(list.get(i));
            level1Temp.add(list.get(i));
        }
        System.out.println();
    }

    public static void printAfterArrangement() {

        System.out.println("\nAfter Arrangement \n");

        // Menaikan Level
        ArrayList<Cage> tempvar = level3Temp;

        level3Temp = level2Temp;
        level2Temp = level1Temp;
        level1Temp = tempvar;
        ///////////////////////////////

        // Membalik urutan per level

        ArrayList<Cage> level3Temp2, level2Temp2, level1Temp2;
        level1Temp2 = (ArrayList<Cage>) level1Temp.clone();
        level2Temp2 = (ArrayList<Cage>) level2Temp.clone();
        level3Temp2 = (ArrayList<Cage>) level3Temp.clone();

        // Membalikan Level 3
        for (int i = 0; i < level3Temp.size(); i++) {
            level3Temp.set(i, level3Temp2.get(level3Temp.size() - i - 1));
        }

        // Membalikan Level 2
        for (int i = 0; i < level2Temp.size(); i++) {
            level2Temp.set(i, level2Temp2.get(level2Temp.size() - i - 1));
        }

        // Membalikan Level 1
        for (int i = 0; i < level1Temp.size(); i++) {
            level1Temp.set(i, level1Temp2.get(level1Temp.size() - i - 1));
        }

        ///////////////////////////////

        // Print per level dan clear setelah print
        System.out.print("Level 3 : ");
        for (Cage i : level3Temp) {
            System.out.print(i + ",");
        }
        level3Temp.clear();

        System.out.println();
        System.out.print("Level 2 : ");
        for (Cage i : level2Temp) {
            System.out.print(i + ",");
        }
        level2Temp.clear();

        System.out.println();
        System.out.print("Level 1 : ");
        for (Cage i : level1Temp) {
            System.out.print(i + ",");
        }
        level1Temp.clear();

        System.out.println();
    }

    // Buat milih mana yang di print before dan after arrangement
    public static void selector(String hewan) {
        if (hewan.equals("cat") && catList.size() > 0) {
            System.out.println("location: indoor");
            printBeforeArrangement(catList);
            printAfterArrangement();

        } else if (hewan.equals("lion") && lionList.size() > 0) {
            System.out.println("location: outdoor");
            printBeforeArrangement(lionList);
            printAfterArrangement();

        } else if (hewan.equals("eagle") && eagleList.size() > 0) {
            System.out.println("location: Outdoor");
            printBeforeArrangement(eagleList);
            printAfterArrangement();

        } else if (hewan.equals("parrot") && parrotList.size() > 0) {
            System.out.println("location: indoor");
            printBeforeArrangement(parrotList);
            printAfterArrangement();

        } else if (hewan.equals("hamster") && hamsterList.size() > 0) {
            System.out.println("location: indoor");
            printBeforeArrangement(hamsterList);
            printAfterArrangement();
        }

    }

    // Tadinya mau buat return hewan tapi gabisa return nya malah parent class (animal) bukan child class
    // karena yang di cage masukinnya animal dan gabisa di casting
    public static Animal nameChecker(String nama, String spesies) {
        if (spesies.equals("cat")) {
            for (Cage i : catList) {
                if (i.getHewan().getName().equals(nama)) {
                    return i.getHewan();
                }
            }
        } else if (spesies.equals("lion")) {
            for (Cage i : lionList) {
                if (i.getHewan().getName().equals(nama)) {
                    return i.getHewan();
                }
            }
        } else if (spesies.equals("parrot")) {
            for (Cage i : parrotList) {
                if (i.getHewan().getName().equals(nama)) {
                    return i.getHewan();
                }
            }
        } else if (spesies.equals("eagle")) {
            for (Cage i : eagleList) {
                if (i.getHewan().getName().equals(nama)) {
                    return i.getHewan();
                }
            }
        } else if (spesies.equals("hamster")) {
            for (Cage i : hamsterList) {
                if (i.getHewan().getName().equals(nama)) {
                    return i.getHewan();
                }
            }
        }
        return null;
    }

    // Print jumlah hewan setiap spesies
    public static void animalNumber() {
        System.out.println("\n\nANIMALS NUMBER:");
        System.out.println("cat:" + catList.size());
        System.out.println("lion:" + lionList.size());
        System.out.println("parrot:" + parrotList.size());
        System.out.println("eagle:" + eagleList.size());
        System.out.println("hamster:" + hamsterList.size() + "\n\n=============================================");
    }


    // Mengambil objek hewan berdasarkan nama dari Array yang berisi semua objek berdasarkan spesies
    public static Cat getCatName(String name) {
        for (Cat i : catArray) {
            if (name.equals(i.getName())) {
                return i;
            }
        }
        return null;
    }

    public static Lion getLionName(String name) {
        for (Lion i : lionArray) {
            if (name.equals(i.getName())) {
                return i;
            }
        }
        return null;
    }

    public static Hamster getHamsterName(String name) {
        for (Hamster i : hamsterArray) {
            if (name.equals(i.getName())) {
                return i;
            }
        }
        return null;
    }

    public static Eagle getEagleName(String name) {
        for (Eagle i : eagleArray) {
            if (name.equals(i.getName())) {
                return i;
            }
        }
        return null;
    }

    public static Parrot getParrotName(String name) {
        for (Parrot i : parrotArray) {
            if (name.equals(i.getName())) {
                return i;
            }
        }
        return null;
    }

    public static int getJumlahBinatang(String spesies) {
        if (spesies.equals("cat")) {
            return catArray.size();
        } else if (spesies.equals("lion")) {
            return lionArray.size();
        } else if (spesies.equals("eagle")) {
            return eagleArray.size();
        } else if (spesies.equals("hamster")) {
            return hamsterArray.size();
        } else if (spesies.equals("parrot")) {
            return parrotArray.size();
        }
        return 0;
    }

}
