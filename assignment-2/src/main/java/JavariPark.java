import animal.*;

import java.util.Scanner;

public class JavariPark {
    public static void main(String[] args) {

        // List spesies yang ada di javari park
        String[] spesies = {"cat", "lion", "eagle", "parrot", "hamster"};
        Scanner input = new Scanner(System.in);

        System.out.println("Welcome to Javari Park!");
        System.out.println("Input the number of animals");

        // Loop setiap spesies yang ada dan meminta info
        for (String i : spesies) {
            System.out.printf("%s : ", i);
            int jumlah = Integer.parseInt(input.nextLine());

            // Jika jumlahnya 0 atau kurang maka tidak meminta informasi
            if (jumlah > 0) {
                System.out.printf("Provide the information of %s(s):\n", i);
                String info = input.nextLine();

                // Pisahkan input berdasarkan koma
                String[] infoArray = info.split(",");

                // Pisahkan input nama dan panjang
                for (String j : infoArray) {
                    String[] infoHewan = j.split("\\|");
                    String nama = infoHewan[0];
                    int length = Integer.parseInt(infoHewan[1]);

                    // Berdasarkan spesies buat objek baru dan tambahkan ke ArrayList di class storage
                    if (i.equals("cat")) {
                        Storage.addCat(new Cat(length, nama));
                    } else if (i.equals("lion")) {
                        Storage.addLion(new Lion(length, nama));
                    } else if (i.equals("eagle")) {
                        Storage.addEagle(new Eagle(length, nama));
                    } else if (i.equals("parrot")) {
                        Storage.addParrot(new Parrot(length, nama));
                    } else if (i.equals("hamster")) {
                        Storage.addHamster(new Hamster(length, nama));
                    }

                }

            }

        }
        System.out.println("Animals has been successfully recorded!");
        System.out.println("=============================================");

        // Print Before and After Arrangement
        for (String i : spesies) {
            Storage.selector(i);
        }

        // Print ANIMAL NUMBER
        Storage.animalNumber();


        //  Animal Visit Section
        String masukan = "0";

        // input 99 akan exit
        while (!masukan.equals("99")) {
            System.out.println("\nWhich animal you want to visit?");
            System.out.println("(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99:exit)");
            masukan = input.nextLine();

            // Jika visit Cat
            if (masukan.equals("1") && Storage.getJumlahBinatang("cat") > 0) {
                doAnimal("cat");
                System.out.println("Back to the office!\n");
            }

            // Jika visit Eagle
            else if (masukan.equals("2") && Storage.getJumlahBinatang("eagle") > 0) {
                doAnimal("eagle");
                System.out.println("Back to the office!\n");
            }
            // Jika visit Hamster
            else if (masukan.equals("3") && Storage.getJumlahBinatang("hamster") > 0) {
                doAnimal("hamster");
                System.out.println("Back to the office!\n");
            }
            // Jika visit Parrot
            else if (masukan.equals("4") && Storage.getJumlahBinatang("parrot") > 0) {
                doAnimal("parrot");
                System.out.println("Back to the office!\n");
            }
            // Jika Visit Lion
            else if (masukan.equals("5") && Storage.getJumlahBinatang("lion") > 0) {
                doAnimal("lion");
                System.out.println("Back to the office!\n");
            } else {
                System.out.println("Hewan tersebut tidak ada di Javari Park\nBack to the office!");
            }
        }

    }

    // Saat melakukan sesuatu ke suatu binatang
    public static void doAnimal(String spesies) {
        Scanner input = new Scanner(System.in);
        System.out.printf("Mention the name of %s you want to visit: ", spesies);
        String nama = input.nextLine();
        if (Storage.nameChecker(nama, spesies) != null) {

            System.out.printf("You are visiting %s (%s) now, what would you like to do?\n", nama, spesies);
            if (spesies.equals("cat")) {

                // Print Behavior
                for (String i : Cat.getBehavior()) {
                    System.out.print(i);
                    System.out.print(" ");
                }

                System.out.println();

                int perintah = Integer.parseInt(input.nextLine());


                    if (perintah == 1) {
                        Storage.getCatName(nama).brush();
                    } else if (perintah == 2) {
                        Storage.getCatName(nama).cuddle();
                    } else {
                    System.out.println("You do nothing...");
                }


            } else if (spesies.equals("lion")) {
                // Print Behavior
                for (String i : Lion.getBehavior()) {
                    System.out.print(i);
                    System.out.print(" ");
                }
                System.out.println();

                int perintah = Integer.parseInt(input.nextLine());

                if (perintah == 1) {
                    Storage.getLionName(nama).hunt();
                } else if (perintah == 2) {
                    Storage.getLionName(nama).brushMane();
                } else if (perintah == 3) {
                    Storage.getLionName(nama).disturb();
                } else {
                    System.out.println("You do nothing...");
                }

            } else if (spesies.equals("hamster")) {

                // Print Behavior
                for (String i : Hamster.getBehavior()) {
                    System.out.print(i);
                    System.out.print(" ");
                }
                System.out.println();
                int perintah = Integer.parseInt(input.nextLine());

                if (perintah == 1) {
                    Storage.getHamsterName(nama).gnaw();
                } else if (perintah == 2) {
                    Storage.getHamsterName(nama).hamsWheel();
                } else {
                    System.out.println("You do nothing...");
                }

            } else if (spesies.equals("eagle")) {

                // Print Behavior
                for (String i : Eagle.getBehavior()) {
                    System.out.print(i);
                    System.out.print(" ");
                }
                System.out.println();
                int perintah = Integer.parseInt(input.nextLine());

                if (perintah == 1) {
                    Storage.getEagleName(nama).OrderToFly();
                } else {
                    System.out.println("You do nothing...");
                }

            } else if (spesies.equals("parrot")) {

                // Print Behavior
                for (String i : Parrot.getBehavior()) {
                    System.out.print(i);
                    System.out.print(" ");
                }
                System.out.println();
                int perintah = Integer.parseInt(input.nextLine());

                if (perintah == 1) {
                    Storage.getParrotName(nama).fly();
                } else if (perintah == 2) {
                    System.out.print("You say: ");
                    Storage.getParrotName(nama).conversation(input.nextLine());
                } else {
                    Storage.getParrotName(nama).doNothing();
                }

            }

        } else {

            // Jika tidak ada binatang dengan nama tersebut
            System.out.printf("There is no %s with that name! Back to the office!\n", spesies);
        }


    }
}
